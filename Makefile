MAKEFLAGS	= --no-print-directory

all:
	@make -C src

clean:
	@make -C src clean

distclean:
	@make -C src distclean

.PHONY: test
test: all
	@cd test && ./test.sh
