# SmallCC

SmallCC is a small C compilateur in OCaml

## TODO

### Etape 1: make an ast

```sh
[Done] Ast
[.30%] Lot of stuff missing in the paser
[.30%] Class visitor
[.30%] PrettyPrint
```

### Etape 2: middle end

```sh
[.00%] Tree language
[.00%] Implement visitor in tree class
```

### Etape 3: back end x86

```sh
[.00%] x86 backend
```

### ???O.O??? Question of dead

Use type of class to represente ast?
Type for the front end and the first visitor and classe for the middle end

## Source

Yacc:

<http://www.lysator.liu.se/c/ANSI-C-grammar-y.html>

Lex:

<http://www.lysator.liu.se/c/ANSI-C-grammar-l.html>
