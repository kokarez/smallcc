type primary_expression =
	  PE_IDENTIFIER of string
	| PE_CONSTANT of string
	| PE_STRING_LITERAL of string
	| PE_Expression of expression
	

and postfix_expression =
	  PE_pri_exp of primary_expression
	| PE_exp of postfix_expression * expression
	| PE_empty of postfix_expression
	| PE_arg_exp_list of postfix_expression * argument_expression_list
	| PE_id of postfix_expression * string
	| PE_prt of postfix_expression * string
	| PE_inc of postfix_expression
	| PE_dec of postfix_expression
	

and argument_expression_list =
	  AEL_exp of assignment_expression
	| AEL_list of argument_expression_list * assignment_expression
	

and unary_expression =
	  UE_post_exp of postfix_expression
	| UE_inc of unary_expression
	| UE_dec of unary_expression
	| UE_cast of unary_operator * cast_expression
	| UE_size_ue of unary_expression
	| UE_size_type of type_name
	

and unary_operator =
	  UO_EPERLUET
	| UO_FOIS
	| UO_PLUS
	| UO_MOIN
	| UO_VAGUE
	| UO_POINTEXCLA
	

and cast_expression =
	  CA_unary_expression of unary_expression
	| CA_type of type_name * cast_expression
	

and multiplicative_expression =
	  ME_exp of cast_expression
	| ME_fois of multiplicative_expression * cast_expression
	| ME_div of multiplicative_expression * cast_expression
	| ME_pourcent of multiplicative_expression * cast_expression
	

and additive_expression =
	  AE_exp of multiplicative_expression
	| AE_plus of additive_expression * multiplicative_expression
	| AE_moin of additive_expression * multiplicative_expression
	

and shift_expression =
	  SE_exp of additive_expression
	| SE_left of shift_expression * additive_expression
	| SE_right of shift_expression * additive_expression
	

and relational_expression =
	  RE_exp of shift_expression
	| RE_left of relational_expression * shift_expression
	| RE_right of relational_expression * shift_expression
	| RE_less of relational_expression * shift_expression
	| RE_gretter of relational_expression * shift_expression
	

and equality_expression =
	  EE_exp of relational_expression
	| EE_eq of equality_expression * relational_expression
	| EE_ne of equality_expression * relational_expression
	

and and_expression =
	  ANE_eq of equality_expression
	| ANE_and of and_expression * equality_expression
	

and exclusive_or_expression =
	  EXEX_and of and_expression
	| EXEX_circon of exclusive_or_expression * and_expression
	

and inclusive_or_expression =
	  INE_exp of exclusive_or_expression
	| INE_pipe of inclusive_or_expression * exclusive_or_expression
	

and logical_and_expression =
	  LAE_exp of inclusive_or_expression
	| LAE_and of logical_and_expression * inclusive_or_expression
	

and logical_or_expression =
	  LOE_exp of logical_and_expression
	| LOE_or of logical_or_expression * logical_and_expression
	

and conditional_expression =
	  COE_exp of logical_or_expression
	| COE_int of logical_or_expression * expression * conditional_expression
	

and assignment_expression =
	  ASE_exp of conditional_expression
	| ASE_op of unary_expression * assignment_operator * assignment_expression
	

and assignment_operator =
	  ASO_EGAL
	| ASO_MUL_ASSIGN
	| ASO_DIV_ASSIGN
	| ASO_MOD_ASSIGN
	| ASO_ADD_ASSIGN
	| ASO_SUB_ASSIGN
	| ASO_LEFT_ASSIGN         
	| ASO_RIGHT_ASSIGN
	| ASO_AND_ASSIGN
	| ASO_XOR_ASSIGN
	| ASO_OR_ASSIGN
	

and expression =
	  E_exp of assignment_expression
	| E_list of expression * assignment_expression
	

and constant_expression =
	 COEX of conditional_expression
	

and declaration =
	  DEC of declaration_specifiers
	| DEC_init of declaration_specifiers * init_declarator_list
	

and declaration_specifiers =
	  DES_class of storage_class_specifier
	| DES_class_dec of storage_class_specifier * declaration_specifiers
	| DES_type of type_specifier
	| DES_type_dec of type_specifier * declaration_specifiers
	| DES_type_qua of type_qualifier
	| DES_type_qua_dec of type_qualifier * declaration_specifiers
	

and init_declarator_list =
	  IDLI_dec of init_declarator
	| IDLI_list of init_declarator_list * init_declarator
	

and init_declarator =
	  IND_dec of declarator
	| IND_dec_init of declarator * cinitializer
	

and storage_class_specifier =
	  SCS_TYPEDEF
	| SCS_EXTERN
	| SCS_STATIC
	| SCS_AUTO
	| SCS_REGISTER
	

and type_specifier =
	  TYPE_VOID
	| TYPE_CHAR
	| TYPE_SHORT
	| TYPE_INT
	| TYPE_LONG
	| TYPE_FLOAT
	| TYPE_DOUBLE
	| TYPE_SIGNED
	| TYPE_UNSIGNED
	| TYPE_struct of struct_or_union_specifier
	| TYPE_enum of enum_specifier
	| TYPE_TYPE_NAME
	

and struct_or_union_specifier =
	  SOUS_struct of struct_or_union * string * struct_declaration_list
	| SOUS_anonyme of struct_or_union * struct_declaration_list
	| SOUS_dec of struct_or_union * string
	

and struct_or_union =
	  TYPE_STRUCT
	| TYPE_UNION
	

and struct_declaration_list =
	  SDL_dec of struct_declaration
	| SDL_list of struct_declaration_list * struct_declaration
	

and struct_declaration =
	 STRUCT_DEC of specifier_qualifier_list * struct_declarator_list
	

and specifier_qualifier_list = 
	  SQL_spec_list of type_specifier * specifier_qualifier_list
	| SQL_spec of type_specifier
	| SQL_qua_list of type_qualifier * specifier_qualifier_list
	| SQL_qua of type_qualifier
	

and struct_declarator_list =
	  STDL_dec of struct_declarator
	| STDL_list of struct_declarator_list * struct_declarator
	

and struct_declarator =
	  SD_dec of declarator
	| SD_constante of constant_expression
	| SD_dec_cons of declarator * constant_expression
	

and enum_specifier =
	  ENUMS_list of enumerator_list
	| ENUMS_name_list of string * enumerator_list
	| ENUMS_name of string
	

and enumerator_list =
	  ENUML_enum of enumerator
	| ENUML_list of enumerator_list * enumerator
	

and enumerator =
	  ENUMERATOR of string
	| ENUMERATOR_constant of string * constant_expression
	

and type_qualifier =
	  TYPE_CONST
	| TYPE_VOLATILE
	

and declarator =
	  DECL_pointer of pointer * direct_declarator
	| DECL_direct of direct_declarator
	

and direct_declarator =
	  DIDE_id of string
	| DIDE_dec of declarator
	| DIDE_cro_const of direct_declarator * constant_expression
	| DIDE_void_cro of direct_declarator
	| DIDE_param_list of direct_declarator * parameter_type_list
	| DIDE_id_list of direct_declarator * identifier_list
	| DIDE_void_para of direct_declarator
	

and pointer =
	  POINTER
	| POINTER_type of type_qualifier_list
	| POINTER_point of pointer
	| POINTER_type_point of type_qualifier_list * pointer
	

and type_qualifier_list =
	  TQL_type of type_qualifier 
	| TQL_list of type_qualifier_list * type_qualifier
	


and parameter_type_list =
	  PTL_list of parameter_list
	| PTL_variatique of parameter_list
	

and parameter_list =
	  PALI_dec of parameter_declaration
	| PALI_list of parameter_list * parameter_declaration
	

and parameter_declaration =
	  PADE_dec_spec of declaration_specifiers * declarator
	| PADE_abs_spec of declaration_specifiers * abstract_declarator
	| PADE_spec of declaration_specifiers
	

and identifier_list =
	  IDL_id of string
	| IDL_list of identifier_list * string
	

and type_name = 
	  TN_list of specifier_qualifier_list
	| TN_list_abs of specifier_qualifier_list * abstract_declarator
	

and abstract_declarator =
	  ABD_pointer of pointer
	| ABD_dec of direct_abstract_declarator
	| ABD_pointer_dec of pointer * direct_abstract_declarator
	

and direct_abstract_declarator =
	  DAD_dec of abstract_declarator
	| DAD_cro_void
	| DAD_const_exp of constant_expression
	| DAD_dec_const_void of direct_abstract_declarator
	| DAD_dec_const of direct_abstract_declarator * constant_expression
	| DAD_para_void
	| DAD_param_list of parameter_type_list
	| DAD_dec_param_void of direct_abstract_declarator
	| DAD_dec_param of direct_abstract_declarator * parameter_type_list
	

and cinitializer =
	  INIT_ass_exp of assignment_expression
	| INIT_list of initializer_list
	

and initializer_list =
	  IL_init of cinitializer
	| IL_list of initializer_list * cinitializer
	

and statement =
	  STAT_label of labeled_statement
	| STAT_compound of compound_statement
	| STAT_exp of expression_statement
	| STAT_selection of selection_statement
	| STAT_iter of iteration_statement
	| STAT_jump of jump_statement
	

and labeled_statement =
	  LS_id of string * statement
	| LS_case of constant_expression * statement
	| LS_default of statement
	

and compound_statement =
	  CS_NONE
	| CS_stat_list of statement_list
	| CS_dec_list of declaration_list
	| CS_dec_stat_list of declaration_list * statement_list
	

and declaration_list =
	  DL_dec of declaration
	| DL_list of declaration_list * declaration
	

and statement_list =
	  STL_stat of statement
	| STL_list of statement_list * statement
	

and expression_statement =
	  ES_NONE
	| ES_EXP of expression
	

and selection_statement =
	  SELS_if of expression * statement
	| SELS_if_else of expression * statement * statement
	| SELS_SWITCH of expression * statement
	

and iteration_statement =
	  ITER_while of expression * statement
	| ITER_do of statement * expression
	| ITER_for of expression_statement * expression_statement * statement
	| ITER_for_exp of expression_statement * expression_statement * expression * statement
	

and jump_statement =
	  JS_GOTO of string 
	| JS_CONTINUE
	| JS_BREAK
	| JS_RETURN_NONE
	| JS_RETURN of expression
	

and translation_unit =
	  TU_dec of external_declaration
	| TU_tans of translation_unit * external_declaration
	| None (* a virer *)
	

and external_declaration =
	  ED_fun_def of function_definition
	| ED_dec of declaration
	

and function_definition =
	  FUN_spec_dec_list of declaration_specifiers * declarator * declaration_list * compound_statement
	| FUN_spec_dec of declaration_specifiers * declarator * compound_statement
	| FUN_dec_list of declarator * declaration_list * compound_statement
	| FUN_dec of declarator * compound_statement
	

