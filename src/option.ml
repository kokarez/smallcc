exception OptionError of string * string

class option argv = 
object(this)

  val mutable files = []
  val mutable options = Hashtbl.create 101

  (***************** Option managment *******************)

  method getFiles = files

  method addOpt key value =
    Hashtbl.add options key value

  method addFile (filepath:string) =
    files <- filepath::files

  method next (l:string list) (f:string -> unit) = match l with
      []   -> raise (OptionError("next", "Option missing"))
    | e::r -> f e; this#rec_parce r

  method rec_parce (l:string list) = match l with
        [] -> ()
      | e::r when e = "-pp"
          -> this#addOpt "prettyprint" "yes"; this#rec_parce r
      | e::r when e = "-o"
          -> this#next r (fun e -> this#addOpt "output" e; ()); ()
      | e::r
          -> this#addFile e; this#rec_parce r; ()
  
  method parse () = match (Array.to_list argv) with
      []      -> ()
    | _::args -> this#rec_parce args

  (*************** method get ******************)

  method is key =
    try
      if (Hashtbl.find options key = "yes") then true else false
    with
    Not_found -> false

  method get key =
    try
      Hashtbl.find options key
    with
    Not_found -> ""

end

let option =
  let a = new option Sys.argv in
    a
