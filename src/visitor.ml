(* This help to manage param *)

type 'a value = None | Some of 'a

exception NoValue
exception NotImplemented of string

let getValue v = match v with
    Some a  -> a
  | None    -> raise NoValue

(* Begin of the visitor class *)

class visitor =
  object(this)

(***************** Accept method for overriding ***********************)

    
method accept_primary_expression (ast:Ast.primary_expression) = ()

method accept_postfix_expression = ()
	
method accept_argument_expression_list = ()

method accept_unary_expression = ()
	
method accept_unary_operator = ()
	
method accept_cast_expression = ()

method accept_multiplicative_expression = ()
	
method accept_additive_expression = ()
	
method accept_shift_expression = ()

method accept_relational_expression = ()

method accept_equality_expression = ()

method accept_and_expression = ()

method accept_exclusive_or_expression = ()

method accept_inclusive_or_expression = ()

method accept_logical_and_expression = ()

method accept_logical_or_expression = ()

method accept_conditional_expression = ()

method accept_assignment_expression = ()

method accept_assignment_operator = ()

method accept_expression = ()

method accept_constant_expression = ()

method accept_declaration = ()

method accept_declaration_specifiers = ()

method accept_init_declarator_list = ()

method accept_init_declarator = ()

method accept_storage_class_specifier = ()

method accept_type_specifier ast = ()

method accept_struct_or_union_specifier = ()

method accept_struct_or_union = ()

method accept_struct_declaration_list = ()

method accept_struct_declaration = ()

method accept_specifier_qualifier_list = () 

method accept_struct_declarator_list = ()

method accept_struct_declarator = ()

method accept_enum_specifier = ()

method accept_enumerator_list = ()

method accept_enumerator = ()

method accept_type_qualifier = ()
	
method accept_declarator = ()

method accept_direct_declarator (ast:Ast.direct_declarator) =  ()

method accept_pointer = ()

method accept_type_qualifier_list = ()

method accept_parameter_type_list = ()

method accept_parameter_list = ()

method accept_parameter_declaration = ()

method accept_identifier_list = ()

method accept_type_name = () 

method accept_abstract_declarator = ()

method accept_direct_abstract_declarator = ()

method accept_cinitializer = ()

method accept_initializer_list = ()

method accept_statement = ()

method accept_labeled_statement = ()

method accept_compound_statement dl sl =
  if (dl <> None) then this#visite_declaration_list (getValue dl);
  if (sl <> None) then this#visite_statement_list (getValue sl)

method accept_declaration_list = ()

method accept_statement_list = ()

method accept_expression_statement = ()
	  
method accept_selection_statement = ()

method accept_iteration_statement = ()

method accept_jump_statement (ast:Ast.jump_statement) = ()

    (* Complet this accept *)
method accept_function_definition decspe dec dec_list cs =
  this#visite_declaration_specifiers (getValue decspe);
  if (decspe <> None) then this#visite_declarator (getValue dec);
  if (dec_list <> None) then this#visite_declaration_list (getValue dec_list);
  this#visite_compound_statement (getValue cs)

  (* useless ? *)
method accept_external_declaration = ()

method accept_translation_unit tu ed =
  if (tu <> None) then this#visite_translation_unit (getValue tu);
  this#visite_external_declaration (getValue ed)

(************************ Visite method *******************************)

method visite_primary_expression ast = 
  this#accept_primary_expression ast	

method visite_postfix_expression ast = match ast with
	  Ast.PE_pri_exp (a) -> this#visite_primary_expression a
	| Ast.PE_exp (a,b) -> ()
	| Ast.PE_empty (a) -> ()
	| Ast.PE_arg_exp_list (a,b) -> ()
	| Ast.PE_id (a,b) -> ()
	| Ast.PE_prt (a,b) -> ()
	| Ast.PE_inc (a) -> ()
	| Ast.PE_dec (a) -> ()
	

method visite_argument_expression_list ast = match ast with
	  Ast.AEL_exp (a) -> ()
	| Ast.AEL_list (a,b) -> ()
	

method visite_unary_expression ast = match ast with
	  Ast.UE_post_exp (a) -> this#visite_postfix_expression a
	| Ast.UE_inc (a) -> ()
	| Ast.UE_dec (a) -> ()
	| Ast.UE_cast (a,b) -> ()
	| Ast.UE_size_ue (a) -> ()
	| Ast.UE_size_type (a) -> ()
	

method visite_unary_operator ast = match ast with
	  Ast.UO_EPERLUET
	| Ast.UO_FOIS
	| Ast.UO_PLUS
	| Ast.UO_MOIN
	| Ast.UO_VAGUE
	| Ast.UO_POINTEXCLA -> ()

	

method visite_cast_expression ast = match ast with
	  Ast.CA_unary_expression (a) -> this#visite_unary_expression a
	| Ast.CA_type (a,b) -> ()
	

method visite_multiplicative_expression ast = match ast with
	  Ast.ME_exp (a) -> this#visite_cast_expression a
	| Ast.ME_fois (a,b) -> ()
	| Ast.ME_div (a,b) -> ()
	| Ast.ME_pourcent (a,b) -> ()
	

method visite_additive_expression ast = match ast with
	  Ast.AE_exp (a) -> this#visite_multiplicative_expression a
	| Ast.AE_plus (a,b) -> ()
	| Ast.AE_moin (a,b) -> ()
	

method visite_shift_expression ast = match ast with
	  Ast.SE_exp (a) -> this#visite_additive_expression a
	| Ast.SE_left (a,b) -> ()
	| Ast.SE_right (a,b) -> ()
	

method visite_relational_expression ast = match ast with
	  Ast.RE_exp (a) -> this#visite_shift_expression a
	| Ast.RE_left (a,b) -> ()
	| Ast.RE_right (a,b) -> ()
	| Ast.RE_less (a,b) -> ()
	| Ast.RE_gretter (a,b) -> ()
	

method visite_equality_expression ast = match ast with
	  Ast.EE_exp (a) -> this#visite_relational_expression a
	| Ast.EE_eq (a,b) -> ()
	| Ast.EE_ne (a,b) -> ()
	

method visite_and_expression ast = match ast with
	  Ast.ANE_eq (a) -> this#visite_equality_expression a
	| Ast.ANE_and (a,b) -> ()
	

method visite_exclusive_or_expression ast = match ast with
	  Ast.EXEX_and (a) -> this#visite_and_expression a
	| Ast.EXEX_circon (a,b) -> ()
	

method visite_inclusive_or_expression ast = match ast with
	  Ast.INE_exp (a) -> this#visite_exclusive_or_expression a
	| Ast.INE_pipe (a,b) -> ()
	

method visite_logical_and_expression ast = match ast with
	  Ast.LAE_exp (a) -> this#visite_inclusive_or_expression a
	| Ast.LAE_and (a,b) -> ()
	

method visite_logical_or_expression ast = match ast with
	  Ast.LOE_exp (a) -> this#visite_logical_and_expression a
	| Ast.LOE_or (a,b) -> ()
	

method visite_conditional_expression ast = match ast with
	  Ast.COE_exp (a) -> this#visite_logical_or_expression a
	| Ast.COE_int (a,b,c) -> ()
	

method visite_assignment_expression ast = match ast with
	  Ast.ASE_exp (a) -> this#visite_conditional_expression a
	| Ast.ASE_op (a,b,c) -> ()
	

method visite_assignment_operator ast = match ast with
	  Ast.ASO_EGAL
	| Ast.ASO_MUL_ASSIGN
	| Ast.ASO_DIV_ASSIGN
	| Ast.ASO_MOD_ASSIGN
	| Ast.ASO_ADD_ASSIGN
	| Ast.ASO_SUB_ASSIGN
	| Ast.ASO_LEFT_ASSIGN         
	| Ast.ASO_RIGHT_ASSIGN
	| Ast.ASO_AND_ASSIGN
	| Ast.ASO_XOR_ASSIGN
	| Ast.ASO_OR_ASSIGN -> ()

	

method visite_expression ast = match ast with
	  Ast.E_exp (a) -> this#visite_assignment_expression a
	| Ast.E_list (a,b) -> ()
	

method visite_constant_expression ast = match ast with
	 Ast.COEX (a) -> ()
	

method visite_declaration ast = match ast with
	  Ast.DEC (a) -> ()
	| Ast.DEC_init (a,b) -> ()
	

method visite_declaration_specifiers ast = match ast with
	  Ast.DES_class (a) ->  raise (NotImplemented "DES_class")
	| Ast.DES_class_dec (a,b) ->  raise (NotImplemented "DES_class_dec")
	| Ast.DES_type (a) ->  this#visite_type_specifier a
	| Ast.DES_type_dec (a,b) ->  raise (NotImplemented "DES_type_dec")
	| Ast.DES_type_qua (a) ->  raise (NotImplemented "DES_type_qua")
	| Ast.DES_type_qua_dec (a,b) ->  raise (NotImplemented "DES_type_qua_dec")
	

method visite_init_declarator_list ast = match ast with
	  Ast.IDLI_dec (a) -> ()
	| Ast.IDLI_list (a,b) -> ()
	

method visite_init_declarator ast = match ast with
	  Ast.IND_dec (a) -> ()
	| Ast.IND_dec_init (a,b) -> ()
	

method visite_storage_class_specifier ast = match ast with
	  Ast.SCS_TYPEDEF
	| Ast.SCS_EXTERN
	| Ast.SCS_STATIC
	| Ast.SCS_AUTO
	| Ast.SCS_REGISTER -> ()

	

method visite_type_specifier ast =
  this#accept_type_specifier ast

	

method visite_struct_or_union_specifier ast = match ast with
	  Ast.SOUS_struct (a,b,c) -> ()
	| Ast.SOUS_anonyme (a,b) -> ()
	| Ast.SOUS_dec (a,b) -> ()
	

method visite_struct_or_union ast = match ast with
	  Ast.TYPE_STRUCT
	| Ast.TYPE_UNION -> ()

	

method visite_struct_declaration_list ast = match ast with
	  Ast.SDL_dec (a) -> ()
	| Ast.SDL_list (a,b) -> ()
	

method visite_struct_declaration ast = match ast with
	 Ast.STRUCT_DEC (a,b) -> ()
	

method visite_specifier_qualifier_list ast = match ast with 
	  Ast.SQL_spec_list (a,b) -> ()
	| Ast.SQL_spec (a) -> ()
	| Ast.SQL_qua_list (a,b) -> ()
	| Ast.SQL_qua (a) -> ()
	

method visite_struct_declarator_list ast = match ast with
	  Ast.STDL_dec (a) -> ()
	| Ast.STDL_list (a,b) -> ()
	

method visite_struct_declarator ast = match ast with
	  Ast.SD_dec (a) -> ()
	| Ast.SD_constante (a) -> ()
	| Ast.SD_dec_cons (a,b) -> ()
	

method visite_enum_specifier ast = match ast with
	  Ast.ENUMS_list (a) -> ()
	| Ast.ENUMS_name_list (a,b) -> ()
	| Ast.ENUMS_name (a) -> ()
	

method visite_enumerator_list ast = match ast with
	  Ast.ENUML_enum (a) -> ()
	| Ast.ENUML_list (a,b) -> ()
	

method visite_enumerator ast = match ast with
	  Ast.ENUMERATOR (a) -> ()
	| Ast.ENUMERATOR_constant (a,b) -> ()
	

method visite_type_qualifier ast = match ast with
	  Ast.TYPE_CONST
	| Ast.TYPE_VOLATILE -> ()
	

method visite_declarator ast = match ast with
	  Ast.DECL_pointer (a,b) -> raise (NotImplemented "DECL_pointer")
	| Ast.DECL_direct (a) -> this#visite_direct_declarator a
	

method visite_direct_declarator ast =
  this#accept_direct_declarator ast

method visite_pointer ast = match ast with
	  Ast.POINTER -> ()
	| Ast.POINTER_type (a) -> ()
	| Ast.POINTER_point (a) -> ()
	| Ast.POINTER_type_point (a,b) -> ()
	

method visite_type_qualifier_list ast = match ast with
	  Ast.TQL_type (a) -> ()
	| Ast.TQL_list (a,b) -> ()
	

(* FIXME must became a accept in pretty *)
method visite_parameter_type_list ast = match ast with
	  Ast.PTL_list (a) -> this#visite_parameter_list a
	| Ast.PTL_variatique (a) -> raise (NotImplemented "PTL_variatique")
	

method visite_parameter_list ast = match ast with
	  Ast.PALI_dec (a) -> this#visite_parameter_declaration a
	| Ast.PALI_list (a,b) -> ()
	

method visite_parameter_declaration ast = match ast with
	  Ast.PADE_dec_spec (a,b) -> raise (NotImplemented "PADE_dec_spec")
	| Ast.PADE_abs_spec (a,b) -> raise (NotImplemented "PADE_abs_spec")
	| Ast.PADE_spec (a) -> this#visite_declaration_specifiers a
	

method visite_identifier_list ast = match ast with
	  Ast.IDL_id (a) -> ()
	| Ast.IDL_list (a,b) -> ()
	

method visite_type_name ast = match ast with 
	  Ast.TN_list (a) -> ()
	| Ast.TN_list_abs (a,b) -> ()
	

method visite_abstract_declarator ast = match ast with
	  Ast.ABD_pointer (a) -> ()
	| Ast.ABD_dec (a) -> ()
	| Ast.ABD_pointer_dec (a,b) -> ()
	

method visite_direct_abstract_declarator ast = match ast with
	  Ast.DAD_dec (a) -> ()
	| Ast.DAD_cro_void-> ()
	| Ast.DAD_const_exp (a) -> ()
	| Ast.DAD_dec_const_void (a) -> ()
	| Ast.DAD_dec_const (a,b) -> ()
	| Ast.DAD_para_void -> ()
	| Ast.DAD_param_list (a) -> ()
	| Ast.DAD_dec_param_void (a) -> ()
  | Ast.DAD_dec_param (a,b) -> ()
	

method visite_cinitializer ast = match ast with
	  Ast.INIT_ass_exp (a) -> ()
	| Ast.INIT_list (a) -> ()
	

method visite_initializer_list ast = match ast with
	  Ast.IL_init (a) -> ()
	| Ast.IL_list (a,b) -> ()
	

method visite_statement ast = match ast with
	  Ast.STAT_label (a) -> raise (NotImplemented "STAT_label")
	| Ast.STAT_compound (a) ->  raise (NotImplemented "STAT_compound")
	| Ast.STAT_exp (a) -> raise (NotImplemented "STAT_exp")
	| Ast.STAT_selection (a) -> raise (NotImplemented "STAT_selection")
	| Ast.STAT_iter (a) -> raise (NotImplemented "STAT_iter")
	| Ast.STAT_jump (a) -> this#visite_jump_statement a
	

method visite_labeled_statement ast = match ast with
	  Ast.LS_id (a,b) -> ()
	| Ast.LS_case (a,b) -> ()
	| Ast.LS_default (a) -> ()
	

method visite_compound_statement ast = match ast with
	    Ast.CS_NONE
      -> ()
    | Ast.CS_stat_list(sl)
      -> this#accept_compound_statement None (Some sl)
    | Ast.CS_dec_list(dl)
      -> raise (NotImplemented "CS_dec_list")
    | Ast.CS_dec_stat_list(dl, sl)
      -> raise (NotImplemented "CS_dec_stat_list")

method visite_declaration_list ast = match ast with
	  Ast.DL_dec (a) -> ()
	| Ast.DL_list (a,b) -> ()
	

method visite_statement_list ast = match ast with
	  Ast.STL_stat (a) -> this#visite_statement a
	| Ast.STL_list (a,b) -> raise (NotImplemented "STL_list")
	

method visite_expression_statement ast = match ast with
	  Ast.ES_NONE -> ()
	| Ast.ES_EXP (a) -> ()
	

method visite_selection_statement ast = match ast with
	  Ast.SELS_if (a,b) -> ()
	| Ast.SELS_if_else (a,b,c) -> ()
	| Ast.SELS_SWITCH (a,b) -> ()
	

method visite_iteration_statement ast = match ast with
	  Ast.ITER_while (a,b) -> ()
	| Ast.ITER_do (a,b) -> ()
	| Ast.ITER_for (a,b,c) -> ()
	| Ast.ITER_for_exp (a,b,c,d) -> ()
	

method visite_jump_statement ast =
  this#accept_jump_statement ast
	

method visite_translation_unit ast = match ast with
	  Ast.TU_dec (a)
      -> this#accept_translation_unit None (Some a) 
	| Ast.TU_tans (a,b)
      -> this#accept_translation_unit (Some a) (Some b)
	| Ast.None -> ()
	

method visite_external_declaration ast = match ast with
    Ast.ED_fun_def(fd)  
    -> this#visite_function_definition fd
	| Ast.ED_dec(dec)     
    -> raise (NotImplemented "ED_dec")

method visite_function_definition ast = match ast with
	  Ast.FUN_spec_dec_list(decspe, dec, dec_list, cs)
    -> this#accept_function_definition (Some decspe) (Some dec) (Some dec_list) (Some cs)
  | Ast.FUN_spec_dec(decspe, dec, cs)
    -> this#accept_function_definition (Some decspe) (Some dec) None (Some cs)
  | Ast.FUN_dec_list(dec, dec_list, cs)
    -> this#accept_function_definition None (Some dec) (Some dec_list) (Some cs)
  | Ast.FUN_dec(dec, cs)
    -> this#accept_function_definition None (Some dec) None (Some cs)

(*************** Some buttyfull method to help ***********************)
(********************** Beurk By Kokarez *****************************)

  method getFuncName ast =
    let dd = match ast with
      Ast.DECL_direct(dd) -> dd | _ -> raise (NotImplemented "GetFuncName") in
      let rec rec_name ast = match ast with
          Ast.DIDE_id(s) -> s
        | Ast.DIDE_param_list(d, _) -> rec_name d
        | _ -> raise (NotImplemented "GetFuncName")
          in
            rec_name dd

  end
