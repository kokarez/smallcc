class compiler = 
object(this) 

  method compile (file:string) =
    print_endline ("AST " ^ file);
    let ast = this#makeAst file in
      begin
        if (Option.option#is "prettyprint") then Prettyprint.visite ast;
      end

  method makeAst (file:string) =
    let file_in = open_in file in
        let wrapper = new Wrapper.wrapper in
            wrapper#from_channel file_in ()

  method run (files:string list) =
    List.iter this#compile files

end

let compiler =
  let a = new compiler in
    a
