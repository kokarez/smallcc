{
   open Parser        (* The type token is defined in parser.mli *)
   exception Eof
   exception BadChar of char
   let get = Lexing.lexeme   
   
   let incr_loc lexbuf delta =
     let pos = lexbuf.Lexing.lex_curr_p in
       lexbuf.Lexing.lex_curr_p <- { pos with
         Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
         Lexing.pos_bol = pos.Lexing.pos_cnum - delta;
       }
       
}

let eol = '\013' '\010' | '\013' | '\010'
let char  = '_' | ['a'-'z'] | ['A'-'Z'] | ['0'-'9']

let D =   ['0'-'9']
let L =   ['a'-'z' 'A'-'Z' '_']

rule token = parse
[' ' '\t'] { token lexbuf }
| ['\n']	 { incr_loc lexbuf 0; token lexbuf }
| "auto"			{AUTO}
| "break"		{BREAK}
| "case"			{CASE}
| "char"			{CHAR}
| "const"		{CONST}
| "continue"		{CONTINUE}
| "default"		{DEFAULT}
| "do"			{DO}
| "double"		{DOUBLE}
| "else"			{ELSE}
| "enum"			{ENUM}
| "extern"		{EXTERN}
| "float"		{FLOAT}
| "for"			{FOR}
| "goto"			{GOTO}
| "if"			{IF}
| "int"			{INT}
| "long"		{LONG}
| "register"{REGISTER}
| "return"	{RETURN}
| "short"		{SHORT}
| "signed"	{SIGNED}
| "sizeof"	{SIZEOF}
| "static"	{STATIC}
| "struct"	{STRUCT}
| "switch"	{SWITCH}
| "typedef"	{TYPEDEF}
| "union"		{UNION}
| "unsigned"{UNSIGNED}
| "void"		{VOID}
| "volatile"{VOLATILE}
| "while"		{WHILE}

(* Not totaly sure part *)

| L(L|D*)*   {IDENTIFIER(get lexbuf)}
| D+        {CONSTANT(get lexbuf)}
| L?['"']L*['"'] {STRING_LITERAL(get lexbuf)}

(* end of Not totaly ... *)

| "..."			{ELLIPSIS}
| ">>="			{RIGHT_ASSIGN}
| "<<="			{LEFT_ASSIGN}
| "+="			{ADD_ASSIGN}
| "-="			{SUB_ASSIGN}
| "*="			{MUL_ASSIGN}
| "/="			{DIV_ASSIGN}
| "%="			{MOD_ASSIGN}
| "&="			{AND_ASSIGN}
| "^="			{XOR_ASSIGN}
| "|="			{OR_ASSIGN}
| ">>"			{RIGHT_OP}
| "<<"			{LEFT_OP}
| "++"			{INC_OP}
| "--"			{DEC_OP}
| "->"			{PTR_OP}
| "&&"			{AND_OP}
| "||"			{OR_OP}
| "<="			{LE_OP}
| ">="			{GE_OP}
| "=="			{EQ_OP}
| "!="			{NE_OP}
| ";"			{POINTVIRGULE}
| ":"			{TWOPOINT}
| "="			{EGAL}
| "{"			{LEFTACO}
| "}"			{RIGHTACO}
| ","			{VIRGULE}
| "("			{LEFTPARA}
| ")"			{RIGHTPARA}
| "["			{LEFTCRO}
| "]"			{RIGHTCRO}
| "."			{POINT}
| "&"			{EPERLUET}
| "!"			{POINTEXCLA}
| "~"			{VAGUE}
| "-"			{MOIN}
| "+"			{PLUS}
| "*"			{FOIS}
| "/"			{DIV}
| "%"			{POURCENT}
| "<"			{LEFTCHE}
| ">"			{RIGHTCHE}
| "^"			{CIRCON}
| "|"			{PIPE}
| "?"			{POINTINTERO}
| eof       {EOF}
| _ as e    { raise (BadChar(e)) }
