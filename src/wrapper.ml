exception ParseError of exn * (int * int * string)

class wrapper =
object(this)

  method from_lexbuf lexbuf () =
    try
      Parser.translation_unit Lexer.token lexbuf
    with
      exn ->
        let lex = lexbuf.Lexing.lex_curr_p in
          raise (ParseError (exn,
                            (lex.Lexing.pos_lnum,
                             lex.Lexing.pos_cnum - lex.Lexing.pos_bol,
                             Lexing.lexeme lexbuf)))

  method wrappe lexbuf () = 
    try
      this#from_lexbuf lexbuf ()
    with
      ParseError(exn, (line, col, tok)) -> 
        Printf.fprintf stderr "Error line:%d col:%d token:\"%s\"\n" line col tok;
        raise exn

  method from_channel fdin () = 
    let lexbuf = Lexing.from_channel fdin in
      this#wrappe lexbuf ()

end
