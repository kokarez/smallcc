let usage () =
  print_endline "./scc FILE [FILE...] [-o output]";
  exit 1

let main () =
  if (Array.length Sys.argv) == 1 then usage ();
  Option.option#parse ();
  Compiler.compiler# run Option.option#getFiles
  

let _ = main ()
