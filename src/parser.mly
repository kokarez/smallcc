%{
   exception NotImplemented of string
%}

%token <string> IDENTIFIER
%token <string> CONSTANT
%token <string> STRING_LITERAL 
%token SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME
%token LEFTPARA RIGHTPARA VIRGULE LEFTACO RIGHTACO POINTVIRGULE EOF TWOPOINT EGAL POINT LEFTCRO RIGHTCRO
%token EPERLUET POINTEXCLA VAGUE MOIN PLUS FOIS DIV POURCENT LEFTCHE RIGHTCHE CIRCON PIPE POINTINTERO

%token TYPEDEF EXTERN STATIC AUTO REGISTER
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token STRUCT UNION ENUM ELLIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%start translation_unit
%type <Ast.translation_unit> translation_unit

%%

primary_expression
	: IDENTIFIER
		{ Ast.PE_IDENTIFIER $1 }
	| CONSTANT
		{ Ast.PE_CONSTANT $1 }
	| STRING_LITERAL
		{ Ast.PE_STRING_LITERAL $1 }
	| LEFTPARA expression RIGHTPARA
		{ raise (NotImplemented "PE_Expression") }
	;

postfix_expression
	: primary_expression
		{ Ast.PE_pri_exp $1 }
	| postfix_expression LEFTCRO expression RIGHTCRO
		{ raise (NotImplemented "PE_exp") }
	| postfix_expression LEFTPARA RIGHTPARA
		{ raise (NotImplemented "PE_empty") }
	| postfix_expression LEFTPARA argument_expression_list RIGHTPARA
		{ raise (NotImplemented "PE_arg_exp_list") }
	| postfix_expression POINT IDENTIFIER
		{ raise (NotImplemented "PE_id") }
	| postfix_expression PTR_OP IDENTIFIER
		{ raise (NotImplemented "PE_prt") }
	| postfix_expression INC_OP
		{ raise (NotImplemented "PE_inc") }
	| postfix_expression DEC_OP
		{ raise (NotImplemented "PE_dec") }
	;

argument_expression_list
	: assignment_expression { Ast.None }
	| argument_expression_list VIRGULE assignment_expression { Ast.None }
	;

unary_expression
	: postfix_expression
		{ Ast.UE_post_exp $1 }
	| INC_OP unary_expression
		{ raise (NotImplemented "UE_inc") }
	| DEC_OP unary_expression
		{ raise (NotImplemented "UE_dec") }
	| unary_operator cast_expression
		{ raise (NotImplemented "UE_cast") }
	| SIZEOF unary_expression
		{ raise (NotImplemented "UE_size_ue") }
	| SIZEOF LEFTPARA type_name RIGHTPARA
		{ raise (NotImplemented "UE_size_type") }
	;

unary_operator
	: EPERLUET
		{ Ast.UO_EPERLUET }
	| FOIS
		{ Ast.UO_FOIS }
	| PLUS
		{ Ast.UO_PLUS }
	| MOIN
		{ Ast.UO_MOIN }
	| VAGUE
		{ Ast.UO_VAGUE }
	| POINTEXCLA
		{ Ast.UO_POINTEXCLA }
	;

cast_expression
	: unary_expression
		{ Ast.CA_unary_expression $1 }
	| LEFTPARA type_name RIGHTPARA cast_expression
		{ raise (NotImplemented "CA_type") }
	;

multiplicative_expression
	: cast_expression
		{ Ast.ME_exp $1 }
	| multiplicative_expression FOIS cast_expression
		{ raise (NotImplemented "ME_fois") }
	| multiplicative_expression DIV cast_expression
		{ raise (NotImplemented "ME_div") }
	| multiplicative_expression POURCENT cast_expression
		{ raise (NotImplemented "ME_pourcent") }
	;

additive_expression
	: multiplicative_expression
		{ Ast.AE_exp $1 }
	| additive_expression PLUS multiplicative_expression
		{ raise (NotImplemented "AE_plus") }
	| additive_expression MOIN multiplicative_expression
		{ raise (NotImplemented "AE_moin") }
	;

shift_expression
	: additive_expression
		{ Ast.SE_exp $1 }
	| shift_expression LEFT_OP additive_expression
		{ raise (NotImplemented "SE_left") }
	| shift_expression RIGHT_OP additive_expression
		{ raise (NotImplemented "SE_right") }
	;

relational_expression
	: shift_expression 
		{ Ast.RE_exp $1 }
	| relational_expression LEFTCHE shift_expression 
		{ raise (NotImplemented "RE_left") }
	| relational_expression RIGHTCHE shift_expression 
		{ raise (NotImplemented "RE_right") }
	| relational_expression LE_OP shift_expression 
		{ raise (NotImplemented "RE_less") }
	| relational_expression GE_OP shift_expression 
		{ raise (NotImplemented "RE_gretter") }
	;

equality_expression
	: relational_expression
		{ Ast.EE_exp $1 }
	| equality_expression EQ_OP relational_expression
		{ raise (NotImplemented "EE_eq") }
	| equality_expression NE_OP relational_expression
		{ raise (NotImplemented "EE_ne") }
	;

and_expression
	: equality_expression 
		{ Ast.ANE_eq $1 }
	| and_expression EPERLUET equality_expression 
		{ raise (NotImplemented "ANE_and") }
	;

exclusive_or_expression
	: and_expression 
		{ Ast.EXEX_and $1 }
	| exclusive_or_expression CIRCON and_expression 
		{ raise (NotImplemented "EXEX_circon") }
	;

inclusive_or_expression
	: exclusive_or_expression
		{ Ast.INE_exp $1 }
	| inclusive_or_expression PIPE exclusive_or_expression
		{ raise (NotImplemented "INE_pipe") }
	;

logical_and_expression
	: inclusive_or_expression 
		{ Ast.LAE_exp $1 }
	| logical_and_expression AND_OP inclusive_or_expression 
		{ raise (NotImplemented "LAE_and") }
	;

logical_or_expression
	: logical_and_expression
		{ Ast.LOE_exp $1 }
	| logical_or_expression OR_OP logical_and_expression
		{ raise (NotImplemented "LOE_or") }
	;

conditional_expression
	: logical_or_expression
		{ Ast.COE_exp $1 }
	| logical_or_expression POINTINTERO expression TWOPOINT conditional_expression
		{ raise (NotImplemented "COE_int") }
	;

assignment_expression
	: conditional_expression
		{ Ast.ASE_exp $1 }
	| unary_expression assignment_operator assignment_expression
		{ raise (NotImplemented "ASE_op") }
	;

assignment_operator
	: EGAL { Ast.None }
	| MUL_ASSIGN { Ast.None }
	| DIV_ASSIGN { Ast.None }
	| MOD_ASSIGN { Ast.None }
	| ADD_ASSIGN { Ast.None }
	| SUB_ASSIGN { Ast.None }
	| LEFT_ASSIGN          { Ast.None }
	| RIGHT_ASSIGN { Ast.None }
	| AND_ASSIGN { Ast.None }
	| XOR_ASSIGN { Ast.None }
	| OR_ASSIGN { Ast.None }
	;

expression
	: assignment_expression
		{ Ast.E_exp $1 }
	| expression VIRGULE assignment_expression
		{ raise (NotImplemented "E_list") }
	;

constant_expression
	: conditional_expression { Ast.None }
	;

declaration
	: declaration_specifiers POINTVIRGULE { Ast.None }
	| declaration_specifiers init_declarator_list POINTVIRGULE { Ast.None }
	;

declaration_specifiers
	: storage_class_specifier 
		{ raise (NotImplemented "DES_class") }
	| storage_class_specifier declaration_specifiers 
		{ raise (NotImplemented "DES_class_dec") }
	| type_specifier 
		{ Ast.DES_type $1 }
	| type_specifier declaration_specifiers 
		{ raise (NotImplemented "DES_type_dec") }
	| type_qualifier 
		{ raise (NotImplemented "DES_type_qua") }
	| type_qualifier declaration_specifiers 
		{ raise (NotImplemented "DES_type_qua_dec") }
	;

init_declarator_list
	: init_declarator { Ast.None }
	| init_declarator_list VIRGULE init_declarator { Ast.None }
	;

init_declarator
	: declarator { Ast.None }
	| declarator EGAL cinitializer { Ast.None }
	;

storage_class_specifier
	: TYPEDEF { Ast.None }
	| EXTERN { Ast.None }
	| STATIC { Ast.None }
	| AUTO { Ast.None }
	| REGISTER { Ast.None }
	;

type_specifier
	: VOID 
		{ Ast.TYPE_VOID }
	| CHAR
		{ Ast.TYPE_CHAR }
	| SHORT
		{ Ast.TYPE_SHORT }
	| INT
		{ Ast.TYPE_INT }
	| LONG
		{ Ast.TYPE_LONG }
	| FLOAT
		{ Ast.TYPE_FLOAT }
	| DOUBLE
		{ Ast.TYPE_DOUBLE }
	| SIGNED
		{ Ast.TYPE_SIGNED }
	| UNSIGNED
		{ Ast.TYPE_UNSIGNED }
	| struct_or_union_specifier
		{ raise (NotImplemented "TYPE_struct") }
	| enum_specifier
		{ raise (NotImplemented "TYPE_enum") }
	| TYPE_NAME
		{ Ast.TYPE_TYPE_NAME }
	;

struct_or_union_specifier
	: struct_or_union IDENTIFIER LEFTACO struct_declaration_list RIGHTACO { Ast.None }
	| struct_or_union LEFTACO struct_declaration_list RIGHTACO { Ast.None }
	| struct_or_union IDENTIFIER { Ast.None }
	;

struct_or_union
	: STRUCT { Ast.None }
	| UNION { Ast.None }
	;

struct_declaration_list
	: struct_declaration { Ast.None }
	| struct_declaration_list struct_declaration { Ast.None }
	;

struct_declaration
	: specifier_qualifier_list struct_declarator_list POINTVIRGULE { Ast.None }
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list { Ast.None }
	| type_specifier { Ast.None }
	| type_qualifier specifier_qualifier_list { Ast.None }
	| type_qualifier { Ast.None }
	;

struct_declarator_list
	: struct_declarator { Ast.None }
	| struct_declarator_list VIRGULE struct_declarator { Ast.None }
	;

struct_declarator
	: declarator { Ast.None }
	| TWOPOINT constant_expression { Ast.None }
	| declarator TWOPOINT constant_expression { Ast.None }
	;

enum_specifier
	: ENUM LEFTACO enumerator_list RIGHTACO { Ast.None }
	| ENUM IDENTIFIER LEFTACO enumerator_list RIGHTACO { Ast.None }
	| ENUM IDENTIFIER { Ast.None }
	;

enumerator_list
	: enumerator { Ast.None }
	| enumerator_list VIRGULE enumerator { Ast.None }
	;

enumerator
	: IDENTIFIER { Ast.None }
	| IDENTIFIER EGAL constant_expression { Ast.None }
	;

type_qualifier
	: CONST { Ast.None }
	| VOLATILE { Ast.None }
	;

declarator
	: pointer direct_declarator { raise (NotImplemented "DECL_point") }
	| direct_declarator { Ast.DECL_direct $1 }
	;

direct_declarator
	: IDENTIFIER 
		{ Ast.DIDE_id $1 }
	| LEFTPARA declarator RIGHTPARA 
		{ raise (NotImplemented "DIDE_dec") }
	| direct_declarator LEFTCRO constant_expression RIGHTCRO 
		{ raise (NotImplemented "DIDE_cro_const") }
	| direct_declarator LEFTCRO RIGHTCRO 
		{ raise (NotImplemented "DIDE_void_cro") }
	| direct_declarator LEFTPARA parameter_type_list RIGHTPARA 
		{ Ast.DIDE_param_list ($1, $3) }
	| direct_declarator LEFTPARA identifier_list RIGHTPARA 
		{ raise (NotImplemented "DIDE_id_list") }
	| direct_declarator LEFTPARA RIGHTPARA 
		{ raise (NotImplemented "DIDE_void_para") }
	;

pointer
	: FOIS { Ast.None }
	| FOIS type_qualifier_list { Ast.None }
	| FOIS pointer { Ast.None }
	| FOIS type_qualifier_list pointer { Ast.None }
	;

type_qualifier_list
	: type_qualifier { Ast.None }
	| type_qualifier_list type_qualifier { Ast.None }
	;


parameter_type_list
	: parameter_list
		{ Ast.PTL_list $1 }
	| parameter_list VIRGULE ELLIPSIS
		{ raise (NotImplemented "PTL_variatique") }
	;

parameter_list
	: parameter_declaration 
		{ Ast.PALI_dec $1 }
	| parameter_list VIRGULE parameter_declaration 
		{ raise (NotImplemented "PALI_list") }
	;

parameter_declaration
	: declaration_specifiers declarator
		{ raise (NotImplemented "PADE_dec_spec") }
	| declaration_specifiers abstract_declarator
		{ raise (NotImplemented "PADE_abs_spec") }
	| declaration_specifiers
		{ Ast.PADE_spec $1 }
	;

identifier_list
	: IDENTIFIER { Ast.None }
	| identifier_list VIRGULE IDENTIFIER { Ast.None }
	;

type_name
	: specifier_qualifier_list { Ast.None }
	| specifier_qualifier_list abstract_declarator { Ast.None }
	;

abstract_declarator
	: pointer { Ast.None }
	| direct_abstract_declarator { Ast.None }
	| pointer direct_abstract_declarator { Ast.None }
	;

direct_abstract_declarator
	: LEFTPARA abstract_declarator RIGHTPARA { Ast.None }
	| LEFTCRO RIGHTCRO { Ast.None }
	| LEFTCRO constant_expression RIGHTCRO { Ast.None }
	| direct_abstract_declarator LEFTCRO RIGHTCRO { Ast.None }
	| direct_abstract_declarator LEFTCRO constant_expression RIGHTCRO { Ast.None }
	| LEFTPARA RIGHTPARA { Ast.None }
	| LEFTPARA parameter_type_list RIGHTPARA { Ast.None }
	| direct_abstract_declarator LEFTPARA RIGHTPARA { Ast.None }
	| direct_abstract_declarator LEFTPARA parameter_type_list RIGHTPARA { Ast.None }
	;

cinitializer
	: assignment_expression { Ast.None }
	| LEFTACO initializer_list RIGHTACO { Ast.None }
	| LEFTACO initializer_list VIRGULE RIGHTACO { Ast.None }
	;

initializer_list
	: cinitializer{ Ast.None }
	| initializer_list VIRGULE cinitializer{ Ast.None }
	;

statement
	: labeled_statement 
		{ raise (NotImplemented "STAT_label") }
	| compound_statement 
		{ raise (NotImplemented "STAT_compound") }
	| expression_statement 
		{ raise (NotImplemented "STAT_exp") }
	| selection_statement 
		{ raise (NotImplemented "STAT_selection") }
	| iteration_statement 
		{ raise (NotImplemented "STAT_iter") }
	| jump_statement 
		{ Ast.STAT_jump $1 }
	;

labeled_statement
	: IDENTIFIER TWOPOINT statement { Ast.None }
	| CASE constant_expression TWOPOINT statement { Ast.None }
	| DEFAULT TWOPOINT statement { Ast.None }
	;

compound_statement
	: LEFTACO RIGHTACO { raise (NotImplemented "CS") }
	| LEFTACO statement_list RIGHTACO { Ast.CS_stat_list $2 }
	| LEFTACO declaration_list RIGHTACO { raise (NotImplemented "CS") }
	| LEFTACO declaration_list statement_list RIGHTACO { raise (NotImplemented "CS") }
	;

declaration_list
	: declaration { Ast.None }
	| declaration_list declaration { Ast.None }
	;

statement_list
	: statement { Ast.STL_stat $1 }
	| statement_list statement { raise (NotImplemented "STL_list") }
	;

expression_statement
	: POINTVIRGULE { Ast.None }
	| expression POINTVIRGULE { Ast.None }
	;

selection_statement
	: IF LEFTPARA expression RIGHTPARA statement { Ast.None }
	| IF LEFTPARA expression RIGHTPARA statement ELSE statement { Ast.None }
	| SWITCH LEFTPARA expression RIGHTPARA statement { Ast.None }
	;

iteration_statement
	: WHILE LEFTPARA expression RIGHTPARA statement { Ast.None }
	| DO statement WHILE LEFTPARA expression RIGHTPARA POINTVIRGULE { Ast.None }
	| FOR LEFTPARA expression_statement expression_statement RIGHTPARA statement { Ast.None }
	| FOR LEFTPARA expression_statement expression_statement expression RIGHTPARA statement { Ast.None }
	;

jump_statement
	: GOTO IDENTIFIER POINTVIRGULE
		{ Ast.JS_GOTO $2 }
	| CONTINUE POINTVIRGULE
		{ Ast.JS_CONTINUE }
	| BREAK POINTVIRGULE
		{ Ast.JS_BREAK }
	| RETURN POINTVIRGULE
		{ Ast.JS_RETURN_NONE }
	| RETURN expression POINTVIRGULE
		{ Ast.JS_RETURN $2 }
	;

translation_unit
	: external_declaration
		{ Ast.TU_dec $1 }
	| translation_unit external_declaration
		{ raise (NotImplemented "TU_tans") }
	;

external_declaration
	: function_definition 
		{ Ast.ED_fun_def $1 }
	| declaration 
		{ raise (NotImplemented "")}
	;

function_definition
	: declaration_specifiers declarator declaration_list compound_statement
		{raise (NotImplemented "FUN_spec_dec_list") }
	| declaration_specifiers declarator compound_statement
		{ Ast.FUN_spec_dec ($1, $2, $3) }
	| declarator declaration_list compound_statement
		{ raise (NotImplemented "FUN_dec_list") }
	| declarator compound_statement
		{ Ast.FUN_dec ($1, $2) }
	;

%%
