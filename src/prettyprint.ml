class prettyPrintVisitor =
  object(this)
    
    inherit Visitor.visitor as old

    method accept_primary_expression t = match t with
	      Ast.PE_IDENTIFIER (a) -> print_string a
	    | Ast.PE_CONSTANT (a) -> print_string a
	    | Ast.PE_STRING_LITERAL (a) -> print_string a
	    | Ast.PE_Expression (a) -> () (* FIXME *)

    method accept_jump_statement t = match t with
	      Ast.JS_GOTO (str)     -> print_endline ("goto " ^ str ^ ";")
      | Ast.JS_CONTINUE     -> print_endline ("continue;")
      | Ast.JS_BREAK        -> print_endline ("break;")
      | Ast.JS_RETURN_NONE  -> print_endline ("return;")
      | Ast.JS_RETURN (exp) ->
          begin
            print_string "return ";
            this#visite_expression exp;
            print_endline ";"
          end

    method accept_type_specifier t = match t with
        Ast.TYPE_VOID -> print_string "void"
	    | Ast.TYPE_CHAR -> print_string "char"
	    | Ast.TYPE_SHORT -> print_string "short"
	    | Ast.TYPE_INT -> print_string "int"
	    | Ast.TYPE_LONG -> print_string "long"
	    | Ast.TYPE_FLOAT -> print_string "float"
	    | Ast.TYPE_DOUBLE -> print_string "double"
	    | Ast.TYPE_SIGNED -> print_string "signed"
	    | Ast.TYPE_UNSIGNED -> print_string "unsigned"
	    | Ast.TYPE_struct(a) -> () (* FIXME *)
	    | Ast.TYPE_enum(a) -> () (* FIXME *)
	    | Ast.TYPE_TYPE_NAME -> () (* FIXME *)


    method accept_compound_statement dl sl =
      print_endline ("{");
      old#accept_compound_statement dl sl;
      print_endline ("}")

    method accept_direct_declarator ast = match ast with
	      Ast.DIDE_id (a) -> print_string a
	    | Ast.DIDE_dec (a) -> raise (Visitor.NotImplemented "DIDE_dec")
	    | Ast.DIDE_cro_const (a,b) -> raise (Visitor.NotImplemented "DIDE_cro_const")
	    | Ast.DIDE_void_cro (a) -> raise (Visitor.NotImplemented "DIDE_void_cro")
	    | Ast.DIDE_param_list (a,b) ->
          begin
            this#visite_direct_declarator a;
            print_string "(";
            this#visite_parameter_type_list b;
            print_string ")"
          end
	    | Ast.DIDE_id_list (a,b) -> raise (Visitor.NotImplemented "DIDE_id_list")
	    | Ast.DIDE_void_para (a) -> raise (Visitor.NotImplemented "DIDE_void_para")


  end

let visite ast =
  let a = new prettyPrintVisitor in
    a#visite_translation_unit ast
