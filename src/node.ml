class node =
  object(this)

    (* here put visitor function *)

  end

type nodeValue = None | Some of node

class notYet where =
  object(this)

    inherit node

  end

let newNotYet where () =
  print_endline where;
  let a = new notYet where in
    a

class func = 
  object(this)

    inherit node

    val mutable claration_specifiers = None
    val mutable declarator = None
    val mutable declaration_list = None
    val mutable compound_statement = None

    method init cs d dl cos =
      claration_specifiers <- cs;
      declarator <- d;
      declaration_list <- dl;
      compound_statement <- cos

  end

let newFunc cs d dl cos () = 
  let a = new func in
    a#init cs d dl cos;
    a

class transUnit =
  object(this)

    inherit node

    val mutable translation_unit = None
    val mutable external_declaration = None
    
    method init tu ed =
      translation_unit <- tu;
      external_declaration <- ed
  end

let newTransUnit tu ed () =
  let a = new transUnit in
    a#init tu ed;
    a

