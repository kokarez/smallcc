#/bin/sh

exe=../scc

err=`mktemp`
out=`mktemp`

onefile () {
  $exe $1 1>$out 2>$err
  strerr=`cat $err`
  if [ "$strerr" != "" ]; then
    echo "[FAIL]" $1
    echo "============================================="
    $exe $1 1>/dev/null
    echo "============================================="
    return 0
  fi
  echo "[PASSE]" $1
}

onefile main.c
onefile print.c
onefile static.c

exit 0
